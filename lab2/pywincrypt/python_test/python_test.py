from traceback import print_exc

import wrapper


if __name__ == '__main__':
    try:
        print('Stuff!')

        sig, key = wrapper.sign(b'test_data')

        print('Signature:', sig.hex())
        print('Public key:', key.hex())

        print(wrapper.verify(b'test_data', sig, key))

        print('More stuff!')

    except Exception:
        print_exc()

    finally:
        input('Press any key...')
