"""
Transposition cipher

Algorithm loops over given text one time, swapping characters based on keyword
Keyword is used to define which character to swap current with

e.g. If keyword starts with L, first letter of the message will be swapped
with letter 76 [ord('L')] places to the right. For the second letter of message
second letter of keyword used and so on. When keyword is exhausted, algorithm
returns to its start.

To ensure message being decode-able initial message is appended with whitespaces
till its length is divisible by length of keyword

During decoding, algorithm loops over text and keyword in reverse order
"""


def tc(text: str, keyword: str, decode=False):
    if not decode:
        text += ' ' * (len(text) % len(keyword))

    size = len(text)
    text = list(text)

    kw_size = len(keyword)
    keyword = [ord(ch) for ch in keyword]

    for i in range(size) if not decode else range(size - 1, -1, -1):
        offset = keyword[i % kw_size]
        j = (i + offset) % size

        text[i], text[j] = text[j], text[i]

    text = ''.join(text)

    if decode:
        return text.strip()

    return text


message = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ' \
          'eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut ' \
          'enim ad minim veniam, quis nostrud exercitation ullamco laboris ' \
          'nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ' \
          'in reprehenderit in voluptate velit esse cillum dolore eu fugiat ' \
          'nulla pariatur. Excepteur sint occaecat cupidatat non proident, ' \
          'sunt in culpa qui officia deserunt mollit anim id est laborum.'

secret = 'Lincoln'

encoded = tc(message, secret)

decoded = tc(encoded, secret, True)

print(f'{message=}\n\n{encoded=}\n\n{decoded=}')
